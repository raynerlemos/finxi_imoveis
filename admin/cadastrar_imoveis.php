<?php include('header.php'); ?>
    <div class="content">
      <div class="container-fluid">
        <div class="CadastroMsgSucesso alert alert-success" role="alert" style="margin-top:20px; display:none;">
          <i class="fa fa-check"></i> Cadastro efetuado com sucesso!
        </div>
        <div class="CadastroContent">
          <form action="cadastrarImoveisDo.php" method="post" enctype="multipart/form-data" onsubmit="return(FormCadastro());">
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-logradouro">Logradouro</label>
              <input type="text" class="form-control" name="logradouro" id="CadastroImovel-logradouro" placeholder="Ex.: Rua Antonio da Silva, 24">
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-bairro">Bairro</label>
              <input type="text" class="form-control" name="bairro" id="CadastroImovel-bairro" placeholder="Ex.: Centro">
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-cidade">Cidade</label>
              <input type="text" class="form-control" name="cidade" id="CadastroImovel-cidade" placeholder="Ex.: Rio de Janeiro">
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-uf">UF</label>
              <select class="form-control" name="uf" id="CadastroImovel-uf">
                <option value="">Selecione uma opção</option>
                <option value="RJ">RJ</option>
                <option value="SP">SP</option>
                <option value="ES">ES</option>
                <option value="MG">MG</option>
              </select>
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-cep">CEP</label>
              <input type="text" class="form-control" name="cep" id="CadastroImovel-cep" placeholder="Ex.: 26376-010">
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-qtd-comodos">Quantidade de Cômodos</label>
              <input type="text" class="form-control" name="qtd_comodos" id="CadastroImovel-qtd-comodos" placeholder="Ex.: 2">
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-foto">Foto do Imóvel</label>
              <input type="file" class="form-control" name="foto" id="CadastroImovel-foto">
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-descricao">Descrição</label>
              <textarea class="form-control" name="descricao" id="CadastroImovel-descricao" rows="8" cols="40"></textarea>
            </div>
            <div class="form-group">
              <label class="control-label" for="CadastroImovel-status">Status do Imóvel</label>
              <select class="form-control" name="status" id="CadastroImovel-status">
                <option value="">Escolha uma opção</option>
                <option value="Disponivel">Disponivel</option>
                <option value="Alugado">Alugado</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success btn-block">Cadastrar Imóvel</button><!-- CadastrarImovelBtn -->
            <a href="listar_imoveis.php"class="btn btn-danger btn-block">Cancelar</a>
          </form>
        </div>

      </div>
    </div>
  </body>
</html>

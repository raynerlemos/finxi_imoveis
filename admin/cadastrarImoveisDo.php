<?php
  require_once('../core/crud.class.php');
  $save = new Crud();

  $logradouro = $_POST['logradouro'];
  $bairro = $_POST['bairro'];
  $cidade = $_POST['cidade'];
  $uf = $_POST['uf'];
  $cep = $_POST['cep'];
  $descricao = $_POST['descricao'];
  $qtd_comodos = $_POST['qtd_comodos'];
  $status = $_POST['status'];
  $foto = $_FILES['foto'];

  function getCoordinates($address){
    $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
    $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&key=AIzaSyAVudS2sZJAhwLcP1_0-pBpdGZsjowKPCY&address=$address";
    $response = file_get_contents($url);
    $json = json_decode($response,TRUE); //generate array object from the response from the web
    $coords = [$json['results'][0]['geometry']['location']['lat'], $json['results'][0]['geometry']['location']['lng']];
    return $coords;
  }

  $addressActual = $logradouro .", " .$bairro. ", " .$cidade. ", " .$uf;
  $coords = getCoordinates($addressActual);

  $tabela = 'imoveis';
  $campos = "imoveis_logradouro, imoveis_bairro, imoveis_cidade, imoveis_uf, imoveis_cep, imoveis_lat, imoveis_lng, imoveis_descricao, imoveis_qtd_comodos, imoveis_status";
  $values = "'$logradouro', '$bairro', '$cidade', '$uf', '$cep', '$coords[0]', '$coords[1]', '$descricao', $qtd_comodos, '$status'";
  $save->save($tabela, $campos, $values);

  $lastId = mysql_insert_id();
  $pasta = "../images/";
  $nome = $foto['name'];
  $tmp = $foto['tmp_name'];
  $size = $foto['size'];
  $formato = explode('.',$nome);
  $formato = $formato[1];
  $nome = "IMG_". $lastId . "_" . uniqid() . "." .$formato;

  $upload = move_uploaded_file($tmp, $pasta.$nome);

  $tabela = 'imoveis';
  $values = 'imoveis_foto = "'.$nome.'"';
  $where = 'imoveis_id = '.$lastId;
  $save->update($tabela, $values, $where);

  header('location:listar_imoveis.php');
?>

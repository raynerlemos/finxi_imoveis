<?php
  include('header.php');

  require_once('../core/crud.class.php');
  $crud = new Crud();

  $idImoveis = $_GET['id'];
  $tabela = 'imoveis';
  $campos = '*';
  $where = 'imoveis_id = '.$idImoveis;

  $Imovel = $crud->Select($tabela,$campos,$where);
  $Imovel = mysql_fetch_object($Imovel);
?>

    <div class="content">
      <div class="container-fluid">
        <div class="CadastroMsgSucesso alert alert-success" role="alert" style="margin-top:20px; display:none;">
          <i class="fa fa-check"></i> Alteração efetuada com sucesso!
        </div>
        <div class="CadastroContent">
          <form action="alterarImoveisDo.php" method="post" enctype="multipart/form-data" onsubmit="return(FormAlterar());">
            <input type="hidden" name="id" id="AlterarImovel-id" value="<?php echo $Imovel->imoveis_id; ?>">
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-logradouro">Logradouro</label>
              <input type="text" class="form-control" name="logradouro" id="AlterarImovel-logradouro" placeholder="Ex.: Rua Antonio da Silva, 24" value="<?php echo $Imovel->imoveis_logradouro; ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-bairro">Bairro</label>
              <input type="text" class="form-control" name="bairro" id="AlterarImovel-bairro" placeholder="Ex.: Centro" value="<?php echo $Imovel->imoveis_bairro; ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-cidade">Cidade</label>
              <input type="text" class="form-control" name="cidade" id="AlterarImovel-cidade" placeholder="Ex.: Rio de Janeiro" value="<?php echo $Imovel->imoveis_cidade; ?>" >
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-uf">UF</label>
              <select class="form-control" name="uf" id="AlterarImovel-uf">
                <option value="">Selecione uma opção</option>
                <option value="RJ">RJ</option>
                <option value="SP">SP</option>
                <option value="ES">ES</option>
                <option value="MG">MG</option>
              </select>
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-cep">CEP</label>
              <input type="text" class="form-control" name="cep" id="AlterarImovel-cep" placeholder="Ex.: 26376-010" value="<?php echo $Imovel->imoveis_cep; ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-qtd-comodos">Quantidade de Cômodos</label>
              <input type="text" class="form-control" name="qtd_comodos" id="AlterarImovel-qtd-comodos" placeholder="Ex.: 2" value="<?php echo $Imovel->imoveis_qtd_comodos; ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-foto">Foto do Imóvel</label>
              <input type="file" class="form-control" name="foto" id="AlterarImovel-foto" placeholder="Ex.: 2">
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-descricao">Descrição</label>
              <textarea class="form-control" name="descricao" id="AlterarImovel-descricao" rows="8" cols="40"><?php echo $Imovel->imoveis_descricao; ?></textarea>
            </div>
            <div class="form-group">
              <label class="control-label" for="AlterarImovel-status">Status do Imóvel</label>
              <select class="form-control" name="status" id="AlterarImovel-status">
                <option value="">Escolha uma opção</option>
                <option value="Disponivel">Disponivel</option>
                <option value="Alugado">Alugado</option>
              </select>
            </div>
            <button type="submit" class="btn btn-success btn-block">Alterar Imóvel</button><!-- AlterarImovelBtn -->
            <a href="listar_imoveis.php"class="btn btn-danger btn-block">Cancelar</a>
          </form>
        </div>

      </div>
    </div>

    <script type="text/javascript">
      $(document).ready(function(){
        $('#AlterarImovel-uf').val("<?php echo $Imovel->imoveis_uf; ?>");
        $('#AlterarImovel-status').val("<?php echo $Imovel->imoveis_status; ?>");
      });
    </script>
  </body>
</html>

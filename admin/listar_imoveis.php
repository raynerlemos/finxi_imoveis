<?php include('header.php'); ?>
    <div class="content">
      <div class="container-fluid">
        <div class="ListagemImoveis">
          <?php
            // SELECT * FROM imoveis WHERE id IS NOT NULL;
            require_once('../core/crud.class.php');

            $tabela = 'imoveis';
            $campos = '*';
            $where = 1;
            $crud = new Crud();
            $listagemImoveis = $crud->Select($tabela,$campos,$where);
          ?>
          <div class="table-responsive">
            <table class="table table-hover">
              <tr>
                <th width="70">
                  ID
                </th>
                <th>
                  Logradouro
                </th>
                <th>
                  Bairro
                </th>
                <th>
                  Cidade
                </th>
                <th>
                  UF
                </th>
                <th>
                  CEP
                </th>
                <th>
                  Status do Imóvel
                </th>
                <th>
                  &nbsp;
                </th>
                <th>
                  &nbsp;
                </th>
              </tr>
              <?php
              if($listagemImoveis){
                while($linha = mysql_fetch_object($listagemImoveis)){
                  echo '
                  <tr>
                    <td>
                      '.$linha->imoveis_id.'
                    </td>
                    <td>
                      '.$linha->imoveis_logradouro.'
                    </td>
                    <td>
                      '.$linha->imoveis_bairro.'
                    </td>
                    <td>
                      '.$linha->imoveis_cidade.'
                    </td>
                    <td>
                      '.$linha->imoveis_uf.'
                    </td>
                    <td>
                      '.$linha->imoveis_cep.'
                    </td>
                    <td>
                      <p class="ListagemImoveis-tag-status" data-status="'.$linha->imoveis_status.'">'.$linha->imoveis_status.'</p>
                    </td>
                    <td>
                      <a href="alterar_imoveis.php?id='.$linha->imoveis_id.'"><i class="fa fa-pencil"></i></a>
                    </td>
                    <td>
                      <a class="ExcluirImovelBtn" data-imovel-id="'.$linha->imoveis_id.'"><i class="fa fa-close"></i></a>
                    </td>
                  </tr>
                  ';
                }
              }
              ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>

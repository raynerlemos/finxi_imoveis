<?php
  require_once('../core/crud.class.php');
  $crud = new Crud();

  $id = $_POST['id'];
  $logradouro = $_POST['logradouro'];
  $bairro = $_POST['bairro'];
  $cidade = $_POST['cidade'];
  $uf = $_POST['uf'];
  $cep = $_POST['cep'];
  $descricao = $_POST['descricao'];
  $qtd_comodos = $_POST['qtd_comodos'];
  $status = $_POST['status'];
  $foto = $_FILES['foto'];

  function getCoordinates($address){
    $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
    $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&key=AIzaSyAVudS2sZJAhwLcP1_0-pBpdGZsjowKPCY&address=$address";
    $response = file_get_contents($url);
    $json = json_decode($response,TRUE); //generate array object from the response from the web
    $coords = [$json['results'][0]['geometry']['location']['lat'], $json['results'][0]['geometry']['location']['lng']];
    return $coords;
  }

  $addressActual = $logradouro .", " .$bairro. ", " .$cidade. ", " .$uf;
  $coords = getCoordinates($addressActual);

  // IMAGEM
  if($foto["name"] != ""){
    $pasta = "../images/";
    $nome = $foto['name'];
    $tmp = $foto['tmp_name'];
    $size = $foto['size'];
    $formato = explode('.',$nome);
    $formato = $formato[1];
    $nome = "IMG_". $id . "_" . uniqid() . "." .$formato;
    $upload = move_uploaded_file($tmp, $pasta.$nome);

    $values = 'imoveis_logradouro = "'.$logradouro.'" , imoveis_bairro = "'.$bairro.'" , imoveis_cidade = "'.$cidade.'" , imoveis_uf = "'.$uf.'" , imoveis_cep = "'.$cep.'" , imoveis_lat = "'.$coords[0].'" , imoveis_lng = "'.$coords[1].'" , imoveis_foto = "'.$nome.'" , imoveis_descricao = "'.$descricao.'" , imoveis_qtd_comodos = '.$qtd_comodos.' , imoveis_status = "'.$status.'"';
  }else{
    $values = 'imoveis_logradouro = "'.$logradouro.'" , imoveis_bairro = "'.$bairro.'" , imoveis_cidade = "'.$cidade.'" , imoveis_uf = "'.$uf.'" , imoveis_cep = "'.$cep.'" , imoveis_lat = "'.$coords[0].'" , imoveis_lng = "'.$coords[1].'" , imoveis_descricao = "'.$descricao.'" , imoveis_qtd_comodos = '.$qtd_comodos.' , imoveis_status = "'.$status.'"';
  }

  $tabela = 'imoveis';
  $where = 'imoveis_id = '.$id;

  $crud->update($tabela, $values, $where);

  header('location:listar_imoveis.php');
?>

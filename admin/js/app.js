var _Helper = {
    isEmpty : function(value){
      if(value == "" || typeof value == "undefined"){
        return true;
      }else{
        return false
      }
    }
};

function FormCadastro(){
  var hasError = 0;

  var logradouro = $('#CadastroImovel-logradouro').val();
  if(_Helper.isEmpty(logradouro)){
    $('#CadastroImovel-logradouro').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-logradouro').parents('.form-group').removeClass('has-error');
  }

  var bairro = $('#CadastroImovel-bairro').val();
  if(_Helper.isEmpty(bairro)){
    $('#CadastroImovel-bairro').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-bairro').parents('.form-group').removeClass('has-error');
  }

  var cidade = $('#CadastroImovel-cidade').val();
  if(_Helper.isEmpty(cidade)){
    $('#CadastroImovel-cidade').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-cidade').parents('.form-group').removeClass('has-error');
  }

  var uf = $('#CadastroImovel-uf').val();
  if(_Helper.isEmpty(uf)){
    $('#CadastroImovel-uf').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-uf').parents('.form-group').removeClass('has-error');
  }

  var descricao = $('#CadastroImovel-descricao').val();
  if(_Helper.isEmpty(descricao)){
    $('#CadastroImovel-descricao').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-descricao').parents('.form-group').removeClass('has-error');
  }

  var cep = $('#CadastroImovel-cep').val();
  if(_Helper.isEmpty(cep)){
    $('#CadastroImovel-cep').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-cep').parents('.form-group').removeClass('has-error');
  }

  var qtd_comodos = $('#CadastroImovel-qtd-comodos').val();
  if(_Helper.isEmpty(qtd_comodos)){
    $('#CadastroImovel-qtd-comodos').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-qtd-comodos').parents('.form-group').removeClass('has-error');
  }

  var foto = $('#CadastroImovel-foto').val();
  if(_Helper.isEmpty(foto)){
    $('#CadastroImovel-foto').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-foto').parents('.form-group').removeClass('has-error');
  }

  var status = $('#CadastroImovel-status').val();
  if(_Helper.isEmpty(status)){
    $('#CadastroImovel-status').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#CadastroImovel-status').parents('.form-group').removeClass('has-error');
  }

  var InsertValues = "'" + logradouro + "','" + bairro + "','" + cidade + "','" + uf + "','" + cep + "','" + qtd_comodos + "','" + status + "'";
  var InsertCampos = "imoveis_logradouro, imoveis_bairro, imoveis_cidade, imoveis_uf, imoveis_cep, imoveis_qtd_comodos, imoveis_status";

  if(hasError == 0){
    alert("Imóvel cadastrado com sucesso!");
    return true;

    $("html, body").animate({ scrollTop: 0 }, 100);
    $('.CadastroMsgSucesso').fadeIn();
    setTimeout(function(){
      $('.CadastroMsgSucesso').fadeOut();
    },3000);
  }else{
    return false;
  }
};

function FormAlterar(){
  var hasError = 0;

  var id = $('#AlterarImovel-id').val();

  var logradouro = $('#AlterarImovel-logradouro').val();
  if(_Helper.isEmpty(logradouro)){
    $('#AlterarImovel-logradouro').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-logradouro').parents('.form-group').removeClass('has-error');
  }

  var bairro = $('#AlterarImovel-bairro').val();
  if(_Helper.isEmpty(bairro)){
    $('#AlterarImovel-bairro').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-bairro').parents('.form-group').removeClass('has-error');
  }

  var cidade = $('#AlterarImovel-cidade').val();
  if(_Helper.isEmpty(cidade)){
    $('#AlterarImovel-cidade').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-cidade').parents('.form-group').removeClass('has-error');
  }

  var uf = $('#AlterarImovel-uf').val();
  if(_Helper.isEmpty(uf)){
    $('#AlterarImovel-uf').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-uf').parents('.form-group').removeClass('has-error');
  }

  var descricao = $('#AlterarImovel-descricao').val();
  if(_Helper.isEmpty(descricao)){
    $('#AlterarImovel-descricao').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-descricao').parents('.form-group').removeClass('has-error');
  }

  var cep = $('#AlterarImovel-cep').val();
  if(_Helper.isEmpty(cep)){
    $('#AlterarImovel-cep').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-cep').parents('.form-group').removeClass('has-error');
  }

  var qtd_comodos = $('#AlterarImovel-qtd-comodos').val();
  if(_Helper.isEmpty(qtd_comodos)){
    $('#AlterarImovel-qtd-comodos').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-qtd-comodos').parents('.form-group').removeClass('has-error');
  }

  var foto = $('#AlterarImovel-foto').val();

  var status = $('#AlterarImovel-status').val();
  if(_Helper.isEmpty(status)){
    $('#AlterarImovel-status').parents('.form-group').addClass('has-error');
    hasError++;
  }else{
    $('#AlterarImovel-status').parents('.form-group').removeClass('has-error');
  }

  if(hasError == 0){
    alert('Imóvel alterado com sucesso');
    return true;
  }else{
    return false;
  }
}

$(document).ready(function(){
  // CADASTRO DE IMOVEIS
  $('.ExcluirImovelBtn').click(function(){
    var ConfirmaExclusao = confirm("Deseja excluir este imóvel");
    var id = $(this).attr('data-imovel-id');
    // console.log(id);
    if(ConfirmaExclusao == true){
      $.ajax({
        method: "POST",
        url: "excluirImoveisDo.php",
        data: {
          id : id,
        }
      });
      location.reload();
    }

  });
})

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Painel de Imóveis</title>
    <link rel="stylesheet" href="../core/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../core/font-awesome-4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style-dist.css">
    <script src="js/jquery-3.1.0.min.js" charset="utf-8"></script>
    <script src="js/app.js" charset="utf-8"></script>
  </head>
  <body>
    <div class="header">
      <div class="container-fluid">
        <div class="logo">
          SGI
        </div>
        <div class="menu">
          <div class="op">
            <a href="index.php">Home</a>
          </div>
          <div class="op">
            <a href="cadastrar_imoveis.php">Cadastrar Imóveis</a>
          </div>
          <div class="op">
            <a href="listar_imoveis.php">Listar Imóveis</a>
          </div>
          <div class="op">
            <a href="../index.php">Sair</a>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 19-Ago-2016 às 03:43
-- Versão do servidor: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finxi_imoveis`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `imoveis`
--

CREATE TABLE `imoveis` (
  `imoveis_id` int(11) NOT NULL,
  `imoveis_logradouro` text NOT NULL,
  `imoveis_bairro` text NOT NULL,
  `imoveis_cidade` text NOT NULL,
  `imoveis_uf` varchar(2) NOT NULL,
  `imoveis_cep` varchar(9) NOT NULL,
  `imoveis_descricao` text NOT NULL,
  `imoveis_qtd_comodos` int(11) NOT NULL,
  `imoveis_status` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `imoveis`
--

INSERT INTO `imoveis` (`imoveis_id`, `imoveis_logradouro`, `imoveis_bairro`, `imoveis_cidade`, `imoveis_uf`, `imoveis_cep`, `imoveis_descricao`, `imoveis_qtd_comodos`, `imoveis_status`) VALUES
(1, 'Antonio Luziario, 460', 'Vila Americana', 'Queimados', 'RJ', '26376-010', 'Este é um exemplo de descrição do imóvel. Este imóvel está sendo exemplificado aqui e agora.', 4, 'Alugado'),
(2, 'Rua Anastacia, 33', 'Roncador', 'Queimados', 'RJ', '26376-010', 'Este é um exemplo de descrição do imóvel. Este imóvel está sendo exemplificado aqui e agora.', 4, 'Alugado'),
(3, 'Rua X da Silva', 'Centro', 'Rio de Janeiro', 'RJ', '26376-010', 'Esta Ã© unma descricao', 3, 'Disponivel'),
(4, 'Rua X da Silva', 'Centro', 'Rio de Janeiro', 'RJ', '26376-010', 'Esta Ã© unma descricao', 3, 'Disponivel');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `imoveis`
--
ALTER TABLE `imoveis`
  ADD PRIMARY KEY (`imoveis_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `imoveis`
--
ALTER TABLE `imoveis`
  MODIFY `imoveis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php include('header.php'); ?>
    <div class="content">
      <div class="container-fluid">
        <div class="Imovel-Detalhes">
            <?php
              function calculate_distance($lat1, $lon1, $lat2, $lon2, $unit='N'){
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);

                if ($unit == "K") {
                  $distance = $miles * 1.609344;
                  return round($distance,1);
                }else if($unit == "N") {
                  $distance = $miles * 0.8684;
                  return round($distance,1);
                }else{
                  return round($miles,1);
                }
              }

              require_once('core/crud.class.php');
              $crud = new Crud();

              $id = $_GET['id'];

              $myPosition = ["-22.7155759","-43.5572931"];

              $tabela = 'imoveis';
              $campos = '*';
              $where = 'imoveis_id = '.$id;
              $listagemImoveis = $crud->Select($tabela,$campos,$where);
              $linha = mysql_fetch_object($listagemImoveis);

              if($linha->imoveis_lat != "" && $linha->imoveis_lng != ""){
                $distancia_imovel = calculate_distance($myPosition[0], $myPosition[1], $linha->imoveis_lat, $linha->imoveis_lng, $unit='K');
                $distancia_status = 1;
              }else{
                $distancia_imovel = "";
                $distancia_status = 0;
              }
            ?>
            <div class="Imovel-Img">
              <img src="images/<?php echo $linha->imoveis_foto; ?>" width="350" height="350"/>
            </div>
            <div class="Imovel-Endereco">
              <?php echo $linha->imoveis_logradouro.", ".$linha->imoveis_bairro; ?>
              <span><?php echo "$linha->imoveis_cidade - <b>$linha->imoveis_uf</b>"; ?></span>
            </div>
            <div class="Imovel-Comodos">
              <?php echo $linha->imoveis_qtd_comodos; ?> Cômodos
            </div>
            <div class="Imovel-Distancia" data-distancia-status="<?php echo $distancia_status; ?>">
              <i class="fa fa-map"></i> Está a <?php echo $distancia_imovel; ?>km da sua localização
            </div>
            <div class="Imovel-Descricao">
              <?php echo $linha->imoveis_descricao; ?>
            </div>
            <div class="btn btn-success Imovel-Btn-Alugar" data-id-imovel="<?php echo $linha->imoveis_id; ?>">
              <i class="fa fa-check"></i> Alugar este imóvel agora!
            </div>
            <a href="index.php" class="btn btn-danger Imovel-Btn-Voltar">
              <i class="fa fa-arrow-left"></i> Voltar a listagem de imóveis
            </a>
        </div>
      </div>
    </div>
  </body>
</html>

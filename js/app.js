var _Helper = {
    isEmpty : function(value){
      if(value == "" || typeof value == "undefined"){
        return true;
      }else{
        return false
      }
    },
    rad : function(x){
      return x * Math.PI / 180;
    },
    getDistance : function(p1, p2) {
      var that = this;
      var R = 6378137; // Earth’s mean radius in meter
      console.log(p1);
      console.log(p2);
      var dLat = that.rad(p2.lat - p1.lat);
      var dLong = that.rad(p2.lng - p1.lng);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(that.rad(p1.lat)) * Math.cos(that.rad(p2.lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;

      var km = that.toKm(d)
      return km; // returns the distance in meter
    },
    toKm : function(inMeters){
      var km = inMeters / 1000;
      return km.toFixed(1);
    }
};

var p1 = {
  "lat" : -22.763057,
  "lng" : -43.4677627
};
var p2 = {
  "lat" : -22.7559337,
  "lng" : -43.4684493
};

$(document).ready(function(){

  $('.Imovel-Btn-Alugar').click(function(){
    var id = $(this).attr('data-id-imovel');
    var confirmaAluguel = confirm("Você deseja alugar este imóvel?");
    if(confirmaAluguel == true){
      $.ajax({
        method: "POST",
        url: "alugarImovelDo.php",
        data: {
          id : id
        }
      });
      window.location="index.php";
    }
  });

  $('.Btn-habilita-busca').click(function(){
    var StatusBoxBusca = $('.Busca-Imoveis').attr('data-status-box');
    if(StatusBoxBusca == 'close'){
      $('.Busca-Imoveis .row').slideDown();
      $('.Busca-Imoveis').attr('data-status-box','open');
    }else{
      $('.Busca-Imoveis .row').slideUp();
      $('.Busca-Imoveis').attr('data-status-box','close');
    }
  });

  $('.BtnBuscarImoveis').click(function(){
    var params = "?";
    var logradouro = $('#BuscaImoveis-logradouro').val();
    if(logradouro != '' && typeof logradouro != "undefined"){
      params += "&logradouro="+logradouro;
    }

    var bairro = $('#BuscaImoveis-bairro').val();
    if(bairro != '' && typeof bairro != 'undefined'){
      params += "&bairro="+bairro;
    }

    var cidade = $('#BuscaImoveis-cidade').val();
    if(cidade != '' && typeof cidade != 'undefined'){
      params += "&cidade="+cidade;

    }
    var uf = $('#BuscaImoveis-uf').val();
    if(uf != '' && typeof uf != 'undefined'){
      params += "&uf="+uf;
    }

    var cep = $('#BuscaImoveis-cep').val();
    if(cep != '' && typeof cep != 'undefined'){
      params += "&cep="+cep;
    }

    var qtd_comodos = $('#BuscaImoveis-qtd-comodos').val();
    if(qtd_comodos != '' && typeof qtd_comodos != 'undefined'){
      params += "&qtd_comodos="+qtd_comodos;
    }

    window.location="index.php"+params;

  });

})

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Painel de Imóveis</title>
    <link rel="stylesheet" href="core/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="core/font-awesome-4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style-dist.css">
    <script src="js/jquery-3.1.0.min.js" charset="utf-8"></script>
    <script src="js/app.js" charset="utf-8"></script>
  </head>
  <body>
    <div class="header">
      <div class="container-fluid">
        <div class="logo">
          <a href="index.php">AlugaMais</a>
        </div>
      </div>
    </div>

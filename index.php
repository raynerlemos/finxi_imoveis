<?php

  include('header.php');

  function calculate_distance($lat1, $lon1, $lat2, $lon2, $unit='N'){
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      $distance = $miles * 1.609344;
      return round($distance,1);
    }else if($unit == "N") {
      $distance = $miles * 0.8684;
      return round($distance,1);
    }else{
      return round($miles,1);
    }
  }

  require_once('core/crud.class.php');
  $crud = new Crud();
  $tabela = 'imoveis';
  $campos = '*';

  $myPosition = ["-22.7155759","-43.5572931"];

  $where = 'imoveis_status = "Disponivel" AND imoveis_lat IS NOT NULL AND imoveis_lng IS NOT NULL';
  $sugestoesPorProximidade = $crud->Select($tabela,$campos,$where);
?>
    <div class="content">
      <div class="container-fluid">
        <div class="Busca-Imoveis" data-status-box="close">
          <?php
            if(mysql_num_rows($sugestoesPorProximidade) > 0){
          ?>
          <div class="row ImoveisSugeridos">
            <div class="col-md-12 ImoveisSugeridos-Titulo">
              Sugestões por proximidade
            </div>
            <?php
              while($linhaImovelProximo = mysql_fetch_object($sugestoesPorProximidade)){
                if($linhaImovelProximo->imoveis_lat != "" && $linhaImovelProximo->imoveis_lng != ""){
                  $distancia_imovel = calculate_distance($myPosition[0], $myPosition[1], $linhaImovelProximo->imoveis_lat, $linhaImovelProximo->imoveis_lng, $unit='K');
                  $distancia_status = 1;
                }else{
                  $distancia_imovel = "";
                  $distancia_status = 0;
                }

                if($distancia_status == 1 && $distancia_imovel < 10){
            ?>
            <div class="col-xs-12 col-sm-6 col-md-3 ImovelSugestao-Box" data-id-imovel="<?php echo $linhaImovelProximo->imoveis_id; ?>">
              <div class="Imovel-Endereco">
                <?php echo $linhaImovelProximo->imoveis_logradouro.", ".$linhaImovelProximo->imoveis_bairro; ?>
                <span><?php echo "$linhaImovelProximo->imoveis_cidade - <b>$linhaImovelProximo->imoveis_uf</b>"; ?></span>
              </div>
              <div class="clearfix"></div>
              <div class="Imovel-Distancia" data-distancia-status="<?php echo $distancia_status; ?>">
                <i class="fa fa-map"></i> Está a <?php echo $distancia_imovel; ?>km da sua localização
              </div>
              <div class="Imovel-Btn-Ver">
                <a href="ver_imovel.php?id=<?php echo $linhaImovelProximo->imoveis_id ?>" class="btn btn-success btn-block">
                  <i class="fa fa-arrow-right"></i> Ver Imóvel
                </a>
              </div>
            </div>
            <?php
                }
              }
            ?>
          </div>
          <?php
            }
          ?>
          <div class="row BuscaImoveis-box">
            <div class="col-md-12 BuscaImoveis-Titulo">O que você está procurando?</div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="BuscaImoveis-logradouro">Logradouro</label>
                <input type="text" class="form-control" id="BuscaImoveis-logradouro" placeholder="Ex.: Rua Antonio da Silva, 24">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="BuscaImoveis-bairro">Bairro</label>
                <input type="text" class="form-control" id="BuscaImoveis-bairro" placeholder="Ex.: Centro">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="BuscaImoveis-cidade">Cidade</label>
                <input type="text" class="form-control" id="BuscaImoveis-cidade" placeholder="Ex.: Rio de Janeiro">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="BuscaImoveis-uf">UF</label>
                <select class="form-control" id="BuscaImoveis-uf">
                  <option value="">Selecione uma opção</option>
                  <option value="RJ">RJ</option>
                  <option value="SP">SP</option>
                  <option value="ES">ES</option>
                  <option value="MG">MG</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="BuscaImoveis-cep">CEP</label>
                <input type="text" class="form-control" id="BuscaImoveis-cep" placeholder="Ex.: 26376-010">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="BuscaImoveis-qtd-comodos">Quantidade de Cômodos</label>
                <input type="text" class="form-control" id="BuscaImoveis-qtd-comodos" placeholder="Ex.: 2">
              </div>
            </div>
            <div class="col-md-12">
              <div class="btn btn-success BtnBuscarImoveis">
                <i class="fa fa-search"></i> Buscar Imóveis
              </div>
            </div>
          </div>
          <div class="Btn-habilita-busca">
            <div class="msg-open">
              <i class="fa fa-search"></i> Buscar por imóveis
            </div>
            <div class="msg-close">
              <i class="fa fa-close"></i> Fechar busca por imóveis
            </div>
          </div>
        </div>
        <div class="Listagem-Imoveis">
          <div class="row">
            <?php

              $params = "";
              if(isset($_GET['logradouro'])){
                $params .= "AND imoveis_logradouro LIKE '%" . $_GET['logradouro'] . "%'";
              }
              if(isset($_GET['bairro'])){
                $params .= "AND imoveis_bairro LIKE '%" . $_GET['bairro'] . "%'";
              }
              if(isset($_GET['cidade'])){
                $params .= "AND imoveis_cidade LIKE '%" . $_GET['cidade'] . "%'";
              }
              if(isset($_GET['uf'])){
                $params .= "AND imoveis_uf = '" . $_GET['uf'] . "'";
              }
              if(isset($_GET['qtd_comodos'])){
                $params .= "AND imoveis_qtd_comodos = " . $_GET['qtd_comodos'];
              }

              $where = 'imoveis_status = "Disponivel" '.$params;
              $listagemImoveis = $crud->Select($tabela,$campos,$where);

              if(mysql_num_rows($listagemImoveis) > 0){
                while($linha = mysql_fetch_object($listagemImoveis)){
                  if($linha->imoveis_lat != "" && $linha->imoveis_lng != ""){
                    $distancia_imovel = calculate_distance($myPosition[0], $myPosition[1], $linha->imoveis_lat, $linha->imoveis_lng, $unit='K');
                    $distancia_status = 1;
                  }else{
                    $distancia_imovel = "";
                    $distancia_status = 0;
                  }
            ?>
            <div class="col-xs-12 col-sm-6 col-md-3 Imovel-Box" data-id-imovel="<?php echo $linha->imoveis_id; ?>">
              <div class="Imovel-Img">
                <img src="images/<?php echo $linha->imoveis_foto; ?>" width="82" height="82"/>
              </div>
              <div class="Imovel-Endereco">
                <?php echo $linha->imoveis_logradouro.", ".$linha->imoveis_bairro; ?>
                <span><?php echo "$linha->imoveis_cidade - <b>$linha->imoveis_uf</b>"; ?></span>
              </div>
              <div class="Imovel-Comodos">
                <?php echo $linha->imoveis_qtd_comodos; ?> Cômodos
              </div>
              <div class="clearfix"></div>
              <div class="Imovel-Distancia" data-distancia-status="<?php echo $distancia_status; ?>">
                <i class="fa fa-map"></i> Está a <?php echo $distancia_imovel; ?>km da sua localização
              </div>
              <div class="Imovel-Btn-Ver">
                <a href="ver_imovel.php?id=<?php echo $linha->imoveis_id ?>" class="btn btn-success btn-block">
                  <i class="fa fa-arrow-right"></i> Ver Imóvel
                </a>
              </div>
            </div>
            <?php
                }
              }else{
            ?>
            <div class="col-md-12">
              Não foi encontrado nenhum imóvel
            </div>
            <?php
              }
            ?>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>

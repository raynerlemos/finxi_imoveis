<?php

  class Connection{

    private $host = 'localhost';
    private $user = 'root';
    private $password = '';
    private $db = 'finxi_imoveis';
    private $connection_status = false;

    public function connect(){
      if(!$this->connection_status){
        $SQL_connection = @mysql_connect($this->host,$this->user,$this->password);
        if($SQL_connection){
            $SQL_select_db = @mysql_select_db($this->db,$SQL_connection);
            if($SQL_select_db){
              $this->connection_status = true;
              return true;
            }else{
              $this->connection_status = false;
              return false;
            }
        }else{
          return false;
        }
      }else{
        return false;
      }
    }

    private function disconnect(){
      if($this->connection_status){
        if(@mysql_close()){
          $this->connection_status = false;
          return true;
        }else{
          return false;
        }
      }
    }

  }

?>
